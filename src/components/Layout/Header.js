import React from 'react';
import logo from '../../assets/images/logo.svg';
import './Header.scss';

class Header extends React.Component {
	render() {
		return (
			<header className="header">
				<img src={logo} className="header__logo" alt="logo"/>
				<p>
					Recruitment task "Login" for LiveChat.
				</p>
			</header>
		);
	}
}
export default Header;
