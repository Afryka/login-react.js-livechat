import isEmail from 'validator/lib/isEmail';

export const isEmailAddress = str => isEmail(str);

export const hasLowerCase = str => /[a-z]/.test(str);
export const hasUpperCase = str => /[A-Z]/.test(str);
export const hasDigits = str => /[0-9]/.test(str);
export const hasPasswordMinChars = str => (str.length >= 6);

export const isPasswordStrengthAcceptable = password => {
	return (
		hasPasswordMinChars(password)
		&& hasUpperCase(password)
		&& hasLowerCase(password)
		&& hasDigits(password)
	);
};
