import { BehaviorSubject } from 'rxjs';

const userSubject = new BehaviorSubject(JSON.parse(localStorage.getItem('user')));

export const auth = {
    login,
    user: userSubject.asObservable(),
    get userValue () { return userSubject.value }
};

function login(email, password, remember) {
	const successResponse = {
		statusCode: 200,
		message: 'Login successful!',
		user: {
			email: 'test@test.pl'
		}
	};
	return new Promise((resolve, reject) => {
		let statusCode = null;
		if((email==="test@test.pl") && (password==="Password1")) {
			statusCode = 200;
		} else {
			statusCode = 400;
		}
		setTimeout(() => {
			if (statusCode === 200) {
				if (remember) {
					localStorage.setItem('user', JSON.stringify(successResponse.user));
				}
				userSubject.next(successResponse.user);
				resolve(successResponse.message)
      } else {
				reject({
					error: 'Invalid email or password!',
				})
      }
    }, 2000)
	});
}
