import React from 'react';
import { auth } from '../../services';
import { history } from '../../helpers';
import { isPasswordStrengthAcceptable, isEmailAddress } from '../../utils';
import Loading from '../../components/Shared/Loading';
import './Login.scss';
export default class Login extends React.Component{
	constructor() {
		super()
		this.state = {
			email: '',
			password: '',
			remember: false,
			validationErrors: {email: '', password: '', form: ''},
			isLogined: false,
			loading: false
		}
	}
	handleClearValidation = (event) => {
		const fieldName = event.target.name;
		this.setState(prevState => ({
			validationErrors: {
				...prevState.validationErrors,
				[fieldName]: ''
			}
		}))
	}

	handleCheckValidation = (event) => {
		const fieldName = event.target.name;
		const value = event.target.value;
		let validationErrors = {};

		switch(fieldName) {
			case 'email':
				if (value === '') {
					validationErrors.email = 'This field is required!';
				} else {
					validationErrors.email = isEmailAddress(value) ? '' : 'Invalid email!';
				}
				break;
			case 'password':
				if (value === '') {
					validationErrors.password = 'This field is required!';
				} else {
					validationErrors.password = isPasswordStrengthAcceptable(value) ? '' : 'Invalid password!';
				}
				break;
			default:
				break;
		}

		this.setState(prevState => ({
			validationErrors: {
				...prevState.validationErrors,
				[fieldName]: validationErrors[fieldName]
			}
		}))
	}

	isLoginFormValid = () => {
		const { password, email, loading } = this.state;
		return (
			isEmailAddress(email)
			&& isPasswordStrengthAcceptable(password)
			&& !loading
		);
	}

	handleInputChange = (event) => {
		const name = event.target.name;
		const checked = event.target.checked;
		let value = event.target.value;
		value = (name === 'remember')? checked : value;
		this.setState({[name]: value});
	}

	handleSubmit = (event) => {
		event.preventDefault();
		const { password, email, remember } = this.state;
		this.setState({
			loading: true
		})

		auth.login(email, password, remember)
			.then((message) => {
				this.setState(prevState => ({
					validationErrors: {
						...prevState.validationErrors,
						form: message
					},
					isLogined: true,
					loading: false
				}))
				setTimeout(()=> {
					history.replace('/');
				}, 2000);
			})
			.catch(error => {
				this.setState(prevState => ({
					validationErrors: {
						...prevState.validationErrors,
						form: error.error
					},
					loading: false
				}))
			});
	}

	render() {
		const { validationErrors, isLogined, loading } = this.state;
		return (
			<div className="login-form">
				{validationErrors.form &&
				<span className={`login-form__submit-messages ${ isLogined ? 'login-form__submit-messages--success' : '' }`}>
					{validationErrors.form}
				</span>
				}
				{!isLogined &&
				<div className="login-form__wrapper">
					<div className="login-form__title">Account login</div>
						<form onSubmit={this.handleSubmit}>
							<div className="login-form__group">
								<label htmlFor="email" className="login-form__label">email</label>
								<input type="text" name="email" className="login-form__text-input"
											 onChange={this.handleInputChange}
											 onFocus={this.handleClearValidation}
											 onBlur={this.handleCheckValidation} />
								<span className="login-form__validation-error">{validationErrors.email}</span>
							</div>
							<div className="login-form__group">
								<label htmlFor="password" className="login-form__label">password</label>
								<input type="password" name="password" className="login-form__text-input"
											 onChange={this.handleInputChange}
											 onFocus={this.handleClearValidation}
											 onBlur={this.handleCheckValidation} />
								<span className="login-form__validation-error">{validationErrors.password}</span>
							</div>
							<div className="login-form__group">
								<input type="checkbox" name="remember" id="remember" className="login-form__checkbox-input"
											 onChange={this.handleInputChange} />
								<label htmlFor="remember" className="login-form__label login-form__label--remember">Remember</label>
							</div>
							<div className="login-form__group--submit">
								<button name="submit" type="submit" className="login-form__submit"
												disabled={!this.isLoginFormValid()}
												onClick={this.handleSubmit}>Login</button>
								{loading &&
								<Loading isLoading={this.state.loading}/>
								}
							</div>
						</form>
					</div>
				}
			</div>);
	}
}
