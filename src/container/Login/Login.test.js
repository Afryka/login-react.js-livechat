import React from 'react';
import { shallow } from 'enzyme';
import Login from './Login';
import { auth } from '../../services';


describe('Login inputs validation:',() =>{
	let wrapper;

	it('Input email should copy value to the state.',()=>
	{
		wrapper = shallow(<Login/>);
		wrapper.find('input[type="text"]').simulate('change', {target: {name: 'email', value: 'test@test.pl'}});
		expect(wrapper.state('email')).toEqual('test@test.pl');
	})
	it('Display validation message for empty input email',()=>
	{
		wrapper = shallow(<Login/>);
		wrapper.find('input[type="text"]').simulate('blur', {target: {name: 'email', value: ''}});
		expect(wrapper.containsMatchingElement('This field is required!')).toEqual(true);
	})
	it('Invalid email validation',()=>
	{
		wrapper = shallow(<Login/>);
		wrapper.find('input[type="text"]').simulate('blur', {target: {name: 'email', value: 'test@test'}});
		expect(wrapper.containsMatchingElement('Invalid email!')).toEqual(true);
	})
	it('Input password should copy value to the state.',()=>{
		wrapper = shallow(<Login/>);
		wrapper.find('input[type="password"]').simulate('change', {target: {name: 'password', value: 'Password1'}});
		expect(wrapper.state('password')).toEqual('Password1');
	})
	it('Display validation message for empty input password',()=>
	{
		wrapper = shallow(<Login/>);
		wrapper.find('input[type="password"]').simulate('blur', {target: {name: 'password', value: ''}});
		expect(wrapper.containsMatchingElement('This field is required!')).toEqual(true);
	})
	it('Display validation message for invalid password',()=>{
		wrapper = shallow(<Login/>);
		wrapper.find('input[type="password"]').simulate('blur', {target: {name: 'password', value: 'Password'}});
		expect(wrapper.containsMatchingElement('Invalid password!')).toBe(true);
	})
})

describe('Login submit button:',() =>{
	let wrapper;

	it('Disable submit button when loading',(done)=>{
		wrapper = shallow(<Login/>);
		wrapper.setState({ loading: true }, () => {
			expect(wrapper.find('button').prop('disabled')).toBe(true);
			done();
		});
	})
})

describe('Login mocked requests:',() =>{
	it('Login request with right data',()=>{
		expect.assertions(1);
		return auth.login('test@test.pl', 'Password1').then(message => expect(message).toEqual('Login successful!'));
	})
	it('Login request with wrong email',()=>{
		expect.assertions(1);
		return auth.login('test@test1.pl', 'Password1').catch(error => expect(error.error).toEqual('Invalid email or' +
			' password!'));
	})
	it('Login request with wrong password',()=>{
		expect.assertions(1);
		return auth.login('test@test.pl', 'Password11').catch(error => expect(error.error).toEqual('Invalid email or' +
			' password!'));
	})
})
