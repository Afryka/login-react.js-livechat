import React from 'react';
import { Router, Route, Redirect, Switch } from 'react-router-dom';
import Header from '../../components/Layout/Header'
import Home from '../Home/Home';
import Login from '../Login/Login';
import { history} from '../../helpers'
import { auth } from '../../services/index';

import './App.scss';

function PrivateRoute ({ children, ...rest }) {
	return (
		<Route {...rest} render={({ location }) => (
			auth.userValue ? (
				children
			) : (
				<Redirect
					to={{
						pathname: "/login",
						state: { from: location }
					}}
				/>
			)
		)}/>
	);
}

class App extends React.Component {

	render() {
		return (
			<div className="app">
				<Router history={history}>
					<Header/>
					<div className="container">
						<Switch>
							<Route path="/login" component={Login}/>
							<PrivateRoute path="/">
								<Home/>
							</PrivateRoute>
						</Switch>
					</div>
				</Router>
			</div>
		);
	}
}

export default App;
